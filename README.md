Renders markdown to HTML and displays the HTML in an embedded web browser.

### 3rd Party Libraries

The hard work of the markdown rendering is preformed by .NET libraries. See the information below for information about the libraries.

#### [AngleSharp](https://github.com/AngleSharp/AngleSharp)

A dependency of HtmlSantizer

#### [HtmlSantizer](https://github.com/mganss/HtmlSanitizer)

Sanitizes HTML to make sure no malicious scripts will be executed when the HTML is loaded in a browser

#### [MarkDig](https://github.com/lunet-io/markdig)

Converts Markdown to HTML

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
